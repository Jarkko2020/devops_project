FROM node:10-alpine
WORKDIR /usr/src/app
COPY app.js .
RUN npm init -y && npm install express --save
CMD node app.js


